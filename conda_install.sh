#! /bin/bash
# 

# Download conda distribution

wget -o Downloads/Miniconda3-py310_22.11.1-1-MacOSX-arm64.sh https://repo.anaconda.com/miniconda/Miniconda3-py310_22.11.1-1-MacOSX-arm64.sh

mkdir -p /opt/conda
bash ~/Downloads/Miniconda3-py310_22.11.1-1-MacOSX-arm64.sh -u # enter return and yes for license

conda init 
conda update conda

conda install mamba
mamba init

mamba install poetry 

conda create -n macrobenthos python=3.11
conda activate macrobenthos

mamba install netcdf4 openmpi gfortran xarray
mamba install seaborn cmocean cartopy matplotlib
mamba install openpyxl pandas geopandas
mamba install lxml spyder sympy poetry
mamba install cmake make yq jq

pip install xlsx2csv
#pip install pangaeapy # not available for python11

conda create -n macrobenthos10 python=3.10
conda activate macrobenthos10

mamba install netcdf4=1.5.8 openmpi gfortran xarray
mamba install seaborn cmocean cartopy matplotlib
mamba install openpyxl pandas geopandas
mamba install lxml spyder sympy  poetry
mamba install cmake make  yq jq

pip install xlsx2csv

mamba update --all 

conda create -n schism python=3.11
conda activate schism

mamba install esmf parmetis clang gfortran
mamba install psyplot dask




